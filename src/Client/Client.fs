module Client

open Elmish
open Elmish.React

open Fable.Core.JsInterop
open Fable.Helpers.React
open Fable.Helpers.React.Props
open Shared
open Fulma
open Fable.FontAwesome
open Fable.FontAwesome.Free
open Fulma.Extensions.Wikiki
open ReactEditor
open Thoth.Json

open FSharp.Reflection

importSideEffects "./sass/style.sass"
module Result =
    let toOption = function
        | Ok x -> Some x
        | Error _ -> None

module URI =
    let private compressToEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private decompressFromEncodedURIComponent(_x: string): string  = importMember "./js/util.js"
    let private getURIhash(): string  = importMember "./js/util.js"
    let private setURIhash(_x: string): unit  = importMember "./js/util.js"

    let parseQuery() =
        getURIhash().Split[|'&'|]
        |> Seq.choose (fun s ->
            match s.Split[|'='|] |> Seq.toList with
            | [key; value] -> Some (key, decompressFromEncodedURIComponent value)
            | _ -> None)
        |> Map.ofSeq

    let updateQuery m =
        m |> Map.toSeq |> Seq.map (fun (key, value) -> key + "=" + compressToEncodedURIComponent value) |> String.concat "&"
        |> setURIhash

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
| SetSourceText of string
| DoConvert
| Converted of string
| Error of string

module Server =

    open Shared
    open Fable.Remoting.Client

    /// A proxy you can use to talk to server directly
    let api : IModelApi =
      Remoting.createApi()
      |> Remoting.withRouteBuilder Route.builder
      #if !DEBUG
      |> Remoting.withBaseUrl Config.azureFunctionsUrl
      #endif
      |> Remoting.buildProxy<IModelApi>

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let initialModel = Model.Default
    let query = URI.parseQuery()
    let code = query |> Map.tryFind "code" |> Option.defaultValue ""
    { initialModel with Source = code }, Cmd.ofMsg DoConvert

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | SetSourceText x ->
        let nextModel = { currentModel with Source = x; FSharpEditorState = Loaded "" }
        nextModel, Cmd.none
    | Converted x ->
        let nextModel = { currentModel with FSharpEditorState = Loaded x }
        nextModel, Cmd.none
    | Error x ->
        let nextModel = { currentModel with FSharpEditorState = ConvertError x }
        nextModel, Cmd.none
    | DoConvert ->
        URI.updateQuery (Map.ofSeq ["code", currentModel.Source])
        let response =
            Cmd.ofAsync
                (fun x -> Server.api.convert x)
                currentModel.Source
                (function | Ok x -> Converted x | Result.Error e -> Error e)
                (fun e -> Error e.Message)
        { currentModel with FSharpEditorState = Loading }, response

let githubIssueUri model = Href "https://github.com/jindraivanek/cs2fs/issues/new"

let safeComponents model =
    let cs2fsOnlineLinks =
        span [ ]
           [
             a [ Href "https://gitlab.com/jindraivanek/cs2fs-online" ] [ str "source code" ]
             str ", "
             a [ Href "https://gitlab.com/jindraivanek/cs2fs-online/issues/new" ] [ str "create issue" ]
           ]

    let cs2fsLinks model =
        span [ ]
           [
             a [ Href "https://github.com/jindraivanek/cs2fs" ] [ str "source code" ]
             str ", "
             a [ githubIssueUri model ] [ str "create issue" ]
           ]

    [ p [ ]
        [ strong [] [ str "cs2fs-online" ]
          str "(this page): "
          cs2fsOnlineLinks ]
      p [ ]
        [ strong [] [ str "cs2fs: " ]
          cs2fsLinks model ] ]

let button disabled txt onClick =
    Button.button
        [ Button.IsFullWidth
          Button.Color IsPrimary
          Button.OnClick onClick
          Button.Disabled disabled ]
        [ str txt ]

let notify txt =
    Notification.notification [ Notification.Color IsDanger ] [str txt]

let viewNavbar =
        Navbar.navbar [ Navbar.IsFixedTop ]
                [ Navbar.Brand.div [ ]
                    [ Navbar.Item.a [ ]
                        [ strong [ ]
                            [ sprintf "cs2fs 0.0.0.0.1-not-even-alpha" |> str ] ] ]
                  Navbar.End.div [ ]
                    [ Navbar.Item.div [ ]
                        [ Button.a [ Button.Props [ Href "https://gitlab.com/jindraivanek/cs2fs-online" ]
                                     Button.Color IsWarning ]
                            [ Icon.icon [ ]
                                [ Fa.i [Fa.Brand.Gitlab] [] ]
                              span [ ]
                                [ str "Gitlab" ] ] ] ] ]

let sourceAndFormatted model dispatch =
    let sourceIsEmpty = model.Source.Trim().Length = 0
    let btnFormatDisabled = sourceIsEmpty || model.IsLoading
    let headers =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
                        [ Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "input-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "Type or paste C# code" ] ] ] ]
                          Column.column [ Column.Width(Screen.All, Column.IsHalf) ]
                            [ Message.message [ Message.Props [Id "formatted-message"] ]
                                [ Message.body [ ]
                                    [ Text.div [ Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ]
                                        [ str "F# code" ] ] ] ] ]

    let editors =
        Columns.columns [ Columns.IsGapless ; Columns.IsMultiline ; Columns.CustomClass "is-gapless" ]
            [
              Column.column [] [
                Editor.editor [ Editor.Language "csharp"
                                Editor.IsReadOnly false
                                Editor.Value model.Source
                                Editor.OnChange (SetSourceText >> dispatch) ]
                ]
              Column.column [] [
                  Control.div [Control.IsLoading model.IsLoading; Control.CustomClass "is-large"] [
                      Editor.editor [ Editor.Language "fsharp"
                                      Editor.IsReadOnly true
                                      Editor.Value model.Formatted ]
                      (if model.IsLoading || not model.IsError then div [] []
                       else Button.a
                                [ Button.IsFullWidth
                                  Button.Color IsPrimary
                                  Button.IsOutlined
                                  Button.Disabled btnFormatDisabled
                                  Button.Props [githubIssueUri model]
                                ] [str "Looks wrong? Create an issue!"])
            ] ] ]

    [ headers ; editors; button btnFormatDisabled (if model.IsLoading then "Converting..." else "Convert") (fun ev -> dispatch DoConvert) ]

let footer model =
    Footer.footer [ ]
                  [ Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] (safeComponents model) ]

let view (model : Model) (dispatch : Msg -> unit) =
    div []
        [ viewNavbar
          div [ Class "page-content"]
                  [
                    yield! sourceAndFormatted model dispatch
                    yield footer model
                  ]

        ]


#if DEBUG
open Elmish.Debug
open Elmish.HMR
#endif

Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
|> Program.withHMR
#endif
|> Program.withReact "elmish-app"
#if DEBUG
|> Program.withDebugger
#endif
|> Program.run
