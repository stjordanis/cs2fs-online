module Server

open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2
open Giraffe
open Shared

open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.Extensions.Logging

open Fable.Remoting.Server
open Fable.Remoting.Giraffe

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us

module Result =
    let attempt f =
        try
            Ok <| f()
        with e -> Error e

let lck = obj()

let init() : Task<Model> = task { return Model.Default }

let convert (x: string) = 
    task { 
        return 
            lock lck (fun () -> 
                Result.attempt <| fun () -> cs2fs.Convert.convertText x
                |> Result.mapError (fun exn -> sprintf "Exception: %s Stack Trace: %s" exn.Message exn.StackTrace)
            ) }

let modelApi = {
    init = init >> Async.AwaitTask
    convert = fun x -> convert x |> Async.AwaitTask
}

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue modelApi
    |> Remoting.buildHttpHandler

// code from https://github.com/giraffe-fsharp/Giraffe.AzureFunctions
[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req : HttpRequest, context : ExecutionContext, log : ILogger) =
  let hostingEnvironment = req.HttpContext.GetHostingEnvironment()
  hostingEnvironment.ContentRootPath <- context.FunctionAppDirectory
  let func = Some >> Task.FromResult
  task {
    let! _ = webApp func req.HttpContext
    req.HttpContext.Response.Body.Flush() //workaround https://github.com/giraffe-fsharp/Giraffe.AzureFunctions/issues/1
    } :> Task
